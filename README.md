# goo24gl63

This project provides tools for studying the groups of order 24 as subgroups of GL(6,3).

Copy the content of the text file to the Sagemath cell:

https://sagecell.sagemath.org/

or a Sagemath worksheet in your Cocalc account:

https://cocalc.com/

Then just evaluate the content.

